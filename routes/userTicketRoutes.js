const express = require('express');
const router = express.Router();
const userTicketController = require('../controllers/userTicketController');


router.post('/create', userTicketController.createTicket);
router.get('/', userTicketController.getAllTickets);
router.put('/:id',userTicketController.confirmTicket);

router
    .route('/:id')
    .get(userTicketController.getTicketById)
    .put(userTicketController.updateTicket)
    .delete(userTicketController.deleteTicket)

module.exports = router;
