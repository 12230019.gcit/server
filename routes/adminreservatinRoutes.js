const express = require('express');
const router = express.Router();

const reservationController = require('../controller/reservation');

router.post('/reservations', reservationController.createReservation);
router.get('/reservations', reservationController.getAllReservations);

module.exports = router;