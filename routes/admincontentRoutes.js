const express = require('express');
const { createContent, updateContent, getContent, deleteContent } = require('../controllers/contentController');
const upload = require('../middleware/multer');

const router = express.Router();

router.post('/', upload.array('images'), createContent);
router.put('/:id', upload.array('images'), updateContent);
router.get('/', getContent);
router.delete('/:id', deleteContent);

module.exports = router;