const express = require("express");
const userRoutes = require('./routes/userRoutes');
const userTicketRoutes = require('./routes/userTicketRoutes');
const contentRoutes = require('./routes/admincontentRoutes');
const ticketRoutes = require('./routes/adminticketRoutes');
const cookieParser = require('cookie-parser');
const cors = require("cors");
const bodyParser = require('body-parser');


const app = express();

// Middleware
app.use(bodyParser.json());


const corsOptions = {
    origin: "http://localhost:3000",
    methods: "GET, POST, PUT, DELETE, PATCH, HEAD",
    credentials: true,
};

app.use(cors(corsOptions));
app.use(cookieParser())
app.use(express.json());

app.use('/api/user', userRoutes)

app.use('/api/userTicket', userTicketRoutes)

app.use('/api/content', contentRoutes)

app.use('/api/', ticketRoutes)


module.exports = app