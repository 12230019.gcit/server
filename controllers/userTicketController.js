const Ticket = require('../models/userTicketModel');
const { sendTicketConfirmationEmail } = require('../utils/emailService')

// Create a new ticket
exports.createTicket = async (req, res) => {
    try {
        const ticket = new Ticket(req.body);
        const savedTicket = await ticket.save();
        res.status(201).json(savedTicket);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }


};

// Get all tickets
exports.getAllTickets = async (req, res) => {
    try {
        const tickets = await Ticket.find();
        res.status(200).json(tickets);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Get a single ticket by ID
exports.getTicketById = async (req, res) => {
    try {
        const ticket = await Ticket.findById(req.params.id);
        if (!ticket) {
            return res.status(404).json({ message: 'Ticket not found' });
        }
        res.status(200).json(ticket);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

exports.updateTicket = async (req, res) => {
    try {
        console.log('Received update request for ID:', req.params.id);
        console.log('Update data:', req.body);
        const ticket = await Ticket.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!ticket) {
            return res.status(404).json({ message: 'Ticket not found' });
        }
        res.status(200).json(ticket);
    } catch (err) {
        console.error('Error updating ticket:', err);
        res.status(500).json({ message: err.message });
    }
};


// Delete a ticket by ID
exports.deleteTicket = async (req, res) => {
    try {
        const ticket = await Ticket.findByIdAndDelete(req.params.id);
        if (!ticket) {
            return res.status(404).json({ message: 'Ticket not found' });
        }
        res.status(200).json({ message: 'Ticket deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

exports.confirmTicket = async (req, res) => {
    const ticketId = req.params.id;
    const { confirmed } = req.body;

    try {
        const ticket = await Ticket.findById(ticketId);
        if (!ticket) {
            return res.status(404).json({ success: false, message: 'Ticket not found' });
        }

        ticket.confirmed = confirmed;
        await ticket.save();

        // Send email notification
        if (confirmed) {
            sendTicketConfirmationEmail(ticket.email, ticket);
        }

        res.json({ success: true, message: 'Ticket updated successfully', ticket });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Server error' });
    }
};


