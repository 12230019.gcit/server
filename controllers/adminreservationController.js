const reservations = require('../models/adminreservationModel');

exports.createReservation = (req, res) => {
    const reservation = req.body;
    reservations.push(reservation);
    res.status(201).json({ message: 'Reservation created successfully' });
};

exports.getAllReservations = (req, res) => {
    const confirmedReservations = reservations.filter(reservation => reservation.confirmed);
    res.status(200).json(confirmedReservations);
};
