const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const refreshToken = "1//04qCaQZ8wmFcjCgYIARAAGAQSNwF-L9IraBmCNgFmLuLuowXzcIuKDmM2MDTQtrOg0fwkpEqYbcUE-ugwxK0AscDGe5bs9vKRIy0"
const clientId = "829825032725-pumdptn2eiceaunsqjnjua95tf9cjdj6.apps.googleusercontent.com"
const clientSecret = "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";
const redirectUri = "https://developers.google.com/oauthplayground";

const oAuth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUri);
oAuth2Client.setCredentials({ refresh_token: refreshToken });

const sendMail = async (req, res) => {
    console.log("hello")
    console.log(req.body)
    const { name, email, subject, message } = req.body;
    const accessToken = await oAuth2Client.getAccessToken();
    console.log(accessToken)
    const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            type: "OAuth2",
            user: "namgaywangchuk600@gmail.com",
            clientId,
            clientSecret,
            refreshToken,
            accessToken: accessToken.token,
        },
    });

    const emailTemplate = `
        <h1>Contact Us Message</h1>
        <p>Name: ${name}</p>
        <p>Email: ${email}</p>
        <p>Subject: ${subject}</p>
        <p>Message: ${message}</p>
    `;

    const mailOptions = {
        from: email,
        to: "namgaywangchuk600@gmail.com",
        subject: "contact us message",
        html: emailTemplate,
    };

    try {
        const info = await transporter.sendMail(mailOptions);
        if (info && info.response) {
            res.status(200).json({ msg: "success" });
        } else {
            res.status(400).json({ msg: "Error in sending mail" });
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: "Internal Server Error" });
    }
};

module.exports = { sendMail };
