const Ticket = require("../models/adminTicketModel");

const ticketCreate = async (req, res) => {
    try {
        const { name, age, price } = req.body

        const Newticket = new Ticket({
            name, age, price
        })
        await Newticket.save()

        res.status(200).json({ success: true, message: "Ticket Created Successfully.", Newticket })

    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: "Internal server error", Newticket })
    }
}

const ticketGet = async (req, res) => {
    try {
        const tickets = await Ticket.find()
        if (!tickets) {
            return res.status(404).json({ success: false, message: "Ticket not found" })
        }
        res.status(200).json({ success: true, tickets })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal error" })
    }

}

const ticketDelete = async (req, res) => {
    try {
        const ticketId = req.params.id;
        const deleteticket = await Ticket.findByIdAndDelete(ticketId);
        if (!deleteticket) {
            return res.status(404).json({ success: false, message: 'Ticket Not Found' });
        }
        res.status(200).json({ success: true, message: 'Ticket Deleted Successfully' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
};

module.exports = { ticketCreate, ticketGet, ticketDelete };