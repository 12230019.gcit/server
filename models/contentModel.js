const mongoose = require('mongoose');

const contentSchema = new mongoose.Schema({
    text: {
        type: String,
    },
    images: {
        type: [String]
    },
    category: {
        type: String,
        required: true
    }
});

const Content = mongoose.model('content', contentSchema);
module.exports = Content;