const mongoose = require("mongoose");

const ticketSchema= new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    age:{
        type:String,
        required:true
    }, 
    price:{
        type:String,
        required:true
    },
})

const Ticket= mongoose.model('adminticket',ticketSchema)

module.exports = Ticket
